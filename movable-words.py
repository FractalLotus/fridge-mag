"""
Application to let you move words around on the screen.
"""

from PyQt5.QtGui import QFont, QIcon
from PyQt5.QtCore import QPointF
from PyQt5.QtWidgets import (QGraphicsView, QGraphicsScene, QGraphicsItem, QApplication,
                             QGraphicsSimpleTextItem, QMainWindow, QGridLayout,
                             QPushButton, QLineEdit, QWidget, QGraphicsRectItem, QLabel,
                             QTextEdit)
import random
import numpy


class MovableWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Slide Words")
        #self.setWindowIcon(QIcon('filename.png'))

        grid_layout = QGridLayout()

        self.line_edit = QLineEdit(self)
        self.line_edit.setPlaceholderText("Enter your words here")
        grid_layout.addWidget(self.line_edit, 0, 0)

        self.btn_input_words = QPushButton("Input Words")
        grid_layout.addWidget(self.btn_input_words, 0, 1)
        self.btn_input_words.clicked.connect(self.parse_input_text)
        self.btn_output = QPushButton("Output Sentence")
        grid_layout.addWidget(self.btn_output, 2, 1)
        self.btn_output.clicked.connect(self.move_output_text)

        self.game_window = GameWindow()
        grid_layout.addWidget(self.game_window, 1, 0)

        self.output_text = QTextEdit()
        self.output_text.setFixedSize(self.game_window.sx, 25)
        grid_layout.addWidget(self.output_text, 2, 0)

        widget = QWidget()
        widget.setLayout(grid_layout)
        self.setCentralWidget(widget)

    def move_output_text(self):
        words_in_output_box = self.game_window.output_rect.collidingItems()

        words_in_order = sorted(words_in_output_box, key=lambda x: x.x(), reverse=False)
        word_list = [w.text() for w in words_in_order]
        sentence = " ".join(word_list) + "\n"
        self.output_text.append(sentence)
        for word in words_in_order:
            self.game_window.scene().removeItem(word)

    def parse_input_text(self):
        text_input = self.line_edit.text()
        words = text_input.split()
        for word in words:
            self.game_window.add_word(word)
        self.line_edit.clear()


class GameWindow(QGraphicsView):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.resize(640, 480)
        self.sx = 635
        self.sy = 475
        self.output_y = 50

        scene = QGraphicsScene(0, 0, self.sx, self.sy)
        self.output_rect = QGraphicsRectItem(5, 5, self.sx-10, self.output_y)
        scene.addItem(self.output_rect)
        self.setScene(scene)

    def add_word(self, word_text):
        if len(word_text) > 26:
            word_text = word_text[0:26]
        text = MyBoundedQtext(word_text)
        # Adding to the scene here means the moved signal fires after it has a scene.
        self.scene().addItem(text)
        text.setFont(QFont("Times", 15))
        text.moveBy(random.randint(1, self.sx - text.boundingRect().width()),
                    random.randint(1, self.sy - text.boundingRect().height()))


class MyBoundedQtext(QGraphicsSimpleTextItem):
    def __init__(self, start_text):
        super().__init__(start_text)

        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemIsSelectable, True)
        #self.setFlag(QGraphicsItem.ItemIsFocusable, True)
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)

    def itemChange(self, change, new_position):
        text_scene = self.scene()
        if text_scene is not None and change == QGraphicsItem.ItemPositionChange:
            min_x = text_scene.sceneRect().x()
            min_y = text_scene.sceneRect().y()
            max_x = text_scene.width() - self.boundingRect().width()
            max_y = text_scene.height() - self.boundingRect().height()
            if new_position.x() > max_x or new_position.x() < min_x or \
                    new_position.y() > max_y or new_position.y() < min_y:
                limit_x = numpy.clip(new_position.x(), min_x, max_x)
                limit_y = numpy.clip(new_position.y(), min_y, max_y)
                return QPointF(limit_x, limit_y)
        return super().itemChange(change, new_position)


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)

    gw = MovableWindow()
    gw.show()
    sys.exit(app.exec_())


"""


QGraphicsItem has collidingItems method that use QGraphicsItem.boundingRect() to detect collisions. 
It is implemented for QGraphicsRectItem. So you only need to call it and iterate over items.

class MyBullet(QGraphicsRectItem):
    
    def move(self):
        colliding = self.collidingItems()
        for item in colliding:
            if isinstance(item, Enemy):
                self.scene().removeItem(item)
                self.scene().removeItem(self)
                return


"""