## Name
# Movable Words

## Description
A fun project to let you move words around on the screen. The plan is to later add features for importing text and
saving the new sentences you create.


## Example


## Installation

Copy to the desktop or another folder and double click the exe file.

## Usage


## Support
Submit issues at the Gitlab repository or email me with questions.

## Roadmap
I plan to create a larger writing tools application including this functionality.

## Contributing

## Related links
[Stackoverflow example](https://stackoverflow.com/questions/12213391/python-pyqt-how-can-i-move-my-widgets-on-the-window-with-mouse/12219643#12219643)
[PyQt tutorial](https://www.pythonguis.com/tutorials/pyqt-qgraphics-vector-graphics/)
[QT-5 documentation](https://doc.qt.io/qt-5/qgraphicsitem.html)
[SO ex](https://stackoverflow.com/questions/3548254/restrict-movable-area-of-qgraphicsitem)
[SO ex](https://stackoverflow.com/questions/47216468/restraining-a-qgraphicsitem-using-itemchange)


## Authors and acknowledgment
Brent A. Shields

## License
This project is licensed under GNU GPL V3. Refer to [the license file](LICENSE).

## Project status
Still in the prototype stage.
